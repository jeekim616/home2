import java.util.Arrays;
public class proov107 {
    public static void main(String[] args) {
        int a[] = {1, 7, 2, 2, 3, 1, 0};
        int kust = 1;
        int kuhu = 4;
        int temp = a[kuhu];
        String msg = Arrays.toString(a);
        System.out.println(msg + "\n"); //  -- [1, 7, 2, 2, 3, 1]
        System.arraycopy(a, kust, a, kuhu, 1);
        a[kust] = temp;
        msg = Arrays.toString(a);
        System.out.println(msg + "\n"); //  -- [1, 3, 2, 2, 7, 1]
        binaryInsertionSort(a);
        msg = Arrays.toString(a);
        System.out.println(msg); //         -- [1, 1, 2, 2, 3, 7]
    }

    /**
     * Binary insertion sort.
     *
     * @author Igavene tudeng jeekim616 incl tema eelmine inkarnatsioon, furunkel027
     * @version 0.2
     * @since 1.6
     *
     * @param a
     *           array to be sorted
     */
    public static void binaryInsertionSort(int[] a) {
        // Inspiratsioon: https://www.geeksforgeeks.org/java-program-for-binary-insertion-sort/
        for (int i = 1; i < a.length; i++) {
            int temp = a[i];
            int left = 0;
            int right = i;

            // int middle = Math.abs((Arrays.binarySearch(a, 0, i, temp) +1));
            int middle = mustKunstOtsing(a, 0, i, temp);
            int stepCount = (i - middle);
            System.arraycopy(a, middle, a, (middle + 1), stepCount);
            a[middle] = temp;

        }
    }

    public static int mustKunstOtsing(int a[], int left, int right, int uurimisalune) {
        // inspiratsioon: https://medium.freecodecamp.org/how-to-implement-a-binary-search-algorithm-in-java-without-recursion-67d9337fd75f
        int pivot = 0;
        while (left < right) { // CHANGE!!! LT not LTEQ
            pivot = (left + right) / 2; // CHANGE!!! and no +1 here
            if (a[pivot] <= uurimisalune) { // CHANGE !!! mandatory LTEQ not LT only
                left = pivot + 1;
            } else if (a[pivot] > uurimisalune) {
                right = pivot; // CHANGE!!! and no +1 here
            } // CHANGE!!! no third branch
        }
        return left; // CHANGE!!! left not pivot
    }

}
